#!/bin/bash 

# ROOT=/home/kuleshov/brian-yu
# SPADES=/home/kuleshov/bin/asm/SPAdes-3.5.0-Linux-compiled/bin/spades.py

while read barcode;
do
  [ ! -e $barcode ] || rm -r $barcode;
  mkdir $barcode
  flags=""
  i=1
  ls -1 $ROOT/$barcode | while read f1;
  do
    read f2
    flags="$flags --pe$i-1 $ROOT/$barcode/$f1 --pe$i-2 $ROOT/$barcode/$f2"
    ((i=i+1))
    if [ "$i" == "5" ]
    then
      echo "$SPADES $flags -o ./$barcode --sc -t 4"
    fi
  done
done <barcodes.txt
