#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

# ----------------------------------------------------------------------------

parser = argparse.ArgumentParser()

parser.add_argument('-b', '--barcodes', required=True)
parser.add_argument('-d', '--dir', required=True)
parser.add_argument('-o', '--out', required=True)

args = parser.parse_args()

# ----------------------------------------------------------------------------

with open(args.barcodes) as f:
  barcodes = [line.strip() for line in f]

ctg_profiles = dict()

for outer_bc in barcodes:
  barcode_contigs = list()
  for inner_bc in barcodes:
    profile_path = args.dir + '/' + outer_bc + '/' + inner_bc + '.profile'
    with open(profile_path) as profile:
      for line in profile:
        ctg, length, count, cov = line.strip().split()
        cov = 0.0 if cov < 5.0 else cov
        ctg_name = outer_bc + '-' + ctg
        if ctg_name not in ctg_profiles:
          ctg_profiles[ctg_name] = dict()
        ctg_profiles[ctg_name][inner_bc] = float(cov)

with open(args.out, 'w') as out:
  for ctg, ctg_profile in ctg_profiles.iteritems():
    out.write('%s' % ctg)
    for bc, cov in sorted(ctg_profile.iteritems()):
      out.write('\t%s:%d' % (bc, cov))
    out.write('\n')

