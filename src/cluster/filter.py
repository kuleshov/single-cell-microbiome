#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import re

# ----------------------------------------------------------------------------

parser = argparse.ArgumentParser()

parser.add_argument('-p', '--profile', required=True)
parser.add_argument('-l', '--length', required=True, type=int)
# parser.add_argument('-o', '--out', required=True)

args = parser.parse_args()

# ----------------------------------------------------------------------------

with open(args.profile) as f:
  for line in f:
    fields = line.split()
    m = re.search(r'length_([0-9]+)_cov', fields[0])
    assert m
    length = int(m.group(1))
    if length > args.length:
      print line,
    # print fields[0], length

