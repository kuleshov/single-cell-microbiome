#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import numpy as np

# ----------------------------------------------------------------------------

parser = argparse.ArgumentParser()

parser.add_argument('-p', '--profiles', required=True)

args = parser.parse_args()

# ----------------------------------------------------------------------------

contigs = dict()
singletons = dict()

N_expected = 96

with open(args.profiles) as f:
  for linenum, line in enumerate(f):
    # if linenum > 500:
    #   break
    fields = line.strip().split()
    ctg = fields[0]
    N = len(fields[1:])
    assert N == N_expected
    profile = np.zeros(N,)
    for i, fi in enumerate(fields[1:]):
      bc, cov = fi.split(':')
      if int(cov) > 0 and int(cov) > args.cutoff:
        profile[i] = 1
    if np.count_nonzero(profile) > 3 and np.count_nonzero(profile) < N/2:
      contigs[ctg] = profile
      # print ''.join([str(int(c)) for c in profile])
    else:
      singletons[ctg] = profile

def dist(p1, p2):
  corr = np.dot(p1, p2) / (np.linalg.norm(p1)*np.linalg.norm(p2))
  return corr

def to_str(profile):
  return ''.join([str(int(p)) for p in profile])

for ctg, profile in contigs.iteritems():
  sorted_profiles = sorted(contigs.values(), key=lambda p : dist(p, profile), reverse=True)
  print to_str(profile)
  for pr in sorted_profiles[1:5]:
    print to_str(pr), '%.3f' % dist(pr, profile)
  print
