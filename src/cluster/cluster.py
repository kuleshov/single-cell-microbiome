#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import numpy as np

# from libkuleshov.stats import n50

# ----------------------------------------------------------------------------

parser = argparse.ArgumentParser()

parser.add_argument('-p', '--profiles', required=True)
parser.add_argument('-t', '--cutoff', type=int, default=0)
parser.add_argument('-c', '--clusters', required=True)
parser.add_argument('-s', '--singletons', required=True)

args = parser.parse_args()

# ----------------------------------------------------------------------------

contigs = dict()
singletons = dict()

N_expected = 96

with open(args.profiles) as f:
  for linenum, line in enumerate(f):
    # if linenum > 500:
    #   break
    fields = line.strip().split()
    ctg = fields[0]
    N = len(fields[1:])
    assert N == N_expected
    profile = np.zeros(N,)
    for i, fi in enumerate(fields[1:]):
      bc, cov = fi.split(':')
      if int(cov) > 0 and int(cov) > args.cutoff:
        profile[i] = 1
    if np.count_nonzero(profile) > 3 and np.count_nonzero(profile) < N/2:
      contigs[ctg] = profile
      # print ''.join([str(int(c)) for c in profile])
    else:
      singletons[ctg] = profile

# ----------------------------------------------------------------------------
# compute clusters using greedy strategy

clusters = list()

for ctg, profile in contigs.iteritems():
  # try to find a matching existing cluster
  for cluster in clusters:
    names = cluster[0]
    centroid = cluster[1]
    # print centroid
    # print profile
    corr = np.dot(profile, centroid) / (np.linalg.norm(profile)*np.linalg.norm(centroid))
    if corr > 0.8:
      names.append(ctg)
      m = float(len(names))
      cluster[1] = (m-1)/m * centroid + (1/m) * profile 
      break
  else:
    # create a new cluster
    clusters.append([[ctg], profile])

# print len(clusters)
# cluster_lengths = [len(c[0]) for c in clusters]
# print args.cutoff, n50(cluster_lengths), max(cluster_lengths)

# ----------------------------------------------------------------------------
# write results

with open(args.clusters, 'w') as out:
  for i, (names, centroid) in enumerate(clusters):
    if len(names) > 1:
      out.write('%d' % i)
      for name in names:
        out.write('\t%s' % name)
      out.write('\n')
    else:
      singletons[names[0]] = centroid

with open(args.singletons, 'w') as out:
  for name in singletons:
    out.write('%s\n' % name)

