#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import pysam

# ----------------------------------------------------------------------------

parser = argparse.ArgumentParser()

parser.add_argument('-b', '--bam')
parser.add_argument('-p', '--profile')
parser.add_argument('-c', '--cov-cutoff', type=int, nargs=2)
parser.add_argument('-r', '--read-length', type=int, default=100)

args = parser.parse_args()

# ----------------------------------------------------------------------------

samfile = pysam.AlignmentFile(args.bam, 'rb')
profile = open(args.profile, 'w')

for ctg, length in zip(samfile.references, samfile.lengths):
  count = float(samfile.count(ctg)) 
  cov = count / length * args.read_length
  if cov >= args.cov_cutoff[0] and count >= args.cov_cutoff[1]:
    profile.write('%s\t%d\t%d\t%f\n' % (ctg, length, count, cov))
  else:
    profile.write('%s\t%d\t0\t0\n' % (ctg, length))
