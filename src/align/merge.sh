#!/bin/bash 

# ROOT=/home/kuleshov/brian-yu
# BWA=/home/kuleshov/bin/bwa
# SAMTOOLS=/sw/bin/samtools

while read barcode;
do
  flags=""
  while read innerbarcode;
  do
    echo "$SAMTOOLS merge -f ./$innerbarcode/$barcode.bam ./$innerbarcode/$barcode.*.sorted.bam \
      && rm ./$innerbarcode/$barcode.*.sorted.bam && $SAMTOOLS index ./$innerbarcode/$barcode.bam"
    # if [ ! -e ./$innerbarcode/$barcode.bam ]
    # then
    #   echo "$SAMTOOLS merge -f ./$innerbarcode/$barcode.bam ./$innerbarcode/$barcode.*.sorted.bam \
    #     && rm ./$innerbarcode/$barcode.*.sorted.bam && $SAMTOOLS index ./$innerbarcode/$barcode.bam"
    # fi
  done <barcodes.txt
done <barcodes.txt

# while read barcode;
# do
#   flags=""
#   while read innerbarcode;
#   do
#     if [ ! -e ./$innerbarcode/$barcode.bam ]
#     then
#       echo $SAMTOOLS index ./$innerbarcode/$barcode.bam
#       # rm ./$innerbarcode/$barcode.*.sorted.bam
#     fi
#   done <barcodes.txt
# done <barcodes.txt
