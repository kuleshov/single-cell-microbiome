#!/bin/bash 

# ROOT=/home/kuleshov/brian-yu
# BWA=/home/kuleshov/bin/bwa
# SAMTOOLS=/sw/bin/samtools

while read barcode;
do
  flags=""
  i=1
  ls -1 $ROOT/$barcode | while read f1;
  do
    read f2
    while read innerbarcode;
    do
      if [ ! -e ./$innerbarcode/$barcode.$f1.sorted.bam ]
      then
        echo "$BWA mem -t 4 ./$innerbarcode/scaffolds.fasta $ROOT/$barcode/$f1 $ROOT/$barcode/$f2 | $SAMTOOLS view -bS -q 30 - | $SAMTOOLS sort - ./$innerbarcode/$barcode.$f1.sorted"
      fi
    done <barcodes.txt
  done
done <barcodes.txt
